#Defines function with metric volume conversions
def metricvol():
    vol_unit_from = str(input("What unit are you converting from? (Millilitres, Litres): "))
    vol_unit_to = input("What unit are you converting to? ")
    if vol_unit_from == "Millilitres":
        mL = float(input("What is your measurement? "))
        if vol_unit_to == "Litres":
            print(mL / 1000)
        if vol_unit_to == "Fluid Ounces":
            print(mL * 0.033814)
        if vol_unit_to == "Pints":
            print(mL / 236.5882365)
        if vol_unit_to == "Quarts":
            print(mL / 473.176473)
        if vol_unit_to == "Gallons":
            print(mL / 3785.411784)
    if vol_unit_from == "Litres":
        L = float(input("What is your measurement?: "))
        if vol_unit_to == "Millilitres":
            print(L * 1000)
        if vol_unit_to == "Fluid Ounces":
            print(L * 33.81413)
        if vol_unit_to == "Cups":
            print(L * 4.166667)
        if vol_unit_to == "Pints":
            print(L * 2.113376)
        if vol_unit_to == "Gallon":
            print(L * 0.2641729)