#Imports  metric volume conversion file as a library
import metricvol as met
#Defines a function with customary conversions
def vol():
    ask_met_unit = str(input("Is your unit in metric? (Yes, No): "))    #Asks if converting from metric
    if ask_met_unit == "Yes":
        met.metricvol() #Calls the metricvol function inside the metricvol library
    else:
        vol_unit_from = input("What unit are you converting from? (Fluid Ounces, Cups, Pints, Quarts, Gallons): ")
        vol_unit_to = input("What unit are you converting to? ")
        if vol_unit_from == "Fluid Ounces":
            oz = float(input("What is your measurement? :")) 
            if vol_unit_to == "Millilitres":
                print(oz * 29.57344)    #Return statements are not necessary due to the lack of a loop
            if vol_unit_to == "Litres":
                print(oz * 0.02957344)
            if vol_unit_to == "Cups":
                print(oz * 0.1232227)
            if vol_unit_to == "Pints":
                print(oz * 0.0624998)
            if vol_unit_to == "Gallons":
                print(oz * 0.0078125)
        if vol_unit_from == "Cups":
            cp = float(input("What is your measurement? :"))
            if vol_unit_to == "Millilitres":
                print(cp * 240)
            if vol_unit_to == "Litres":
                print(cp * 0.24)
            if vol_unit_to == "Fluid Ounces":
                print(cp * 8.115391)
            if vol_unit_to == "Pints":
                print(cp * 0.5072103)
            if vol_unit_to == "Quarts":
                print(cp * 0.2536052)
            if vol_unit_to == "Gallons":
                print(cp * 0.06340149)
        if vol_unit_from == "Pints":
            pt = float(input("What is your measurement? :"))
            if vol_unit_to == "Millilitres":
                print(pt * 473.1765)
            if vol_unit_to == "Litres":
                print(pt * 0.4731765)
            if vol_unit_to == "Fluid Ounces":
                print(cp * 16.00005)
            if vol_unit_to == "Cups":
                print(pt * 1.971569)
            if vol_unit_to == "Quarts":
                print(pt * 0.5)
            if vol_unit_to == "Gallons":
                print(pt * 0.1250004)