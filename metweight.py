#Defines metric weight conversions
def weight():
    met_vol_from = input("What is your measurement? (Milligrammes, Grammes, Kilogrammes, Metric Tonnes, Long Tonnes): ")
    met_vol_to = input("What are you converting to? (Milligrammes, Grammes, Kilogrammes, Metric Tonnes, Long Tonnes, Ounces, Pounds, Short Tonnes): ")
    if met_vol_from == "Milligrammes":
        mg = float(input("What is your measurement? "))
        if met_vol_to == "Grammes":
            print(mg * 0.001)
        if met_vol_to == "Kilogrammes":
            print(mg * 0.000001)
        if met_vol_to == "Metric Tonnes":
            print(mg * 0.000000001)
        if met_vol_to == "Long Tonnes":
            print(mg * 0.0000000009842035)
        if met_vol_to == "Ounces":
            print(mg * 0.00003527396)
        if met_vol_to == "Pounds":
            print(mg * 0.000002204623)
        if met_vol_to == "Short Tonnes":
            print(mg * 0.000000001102311)
    if met_vol_from == "Grammes":
        g = float(input("What is your measurement? "))
        if met_vol_to == "Milligrammes":
            print(g * 1000)
        if met_vol_to == "Kilogrammes":
            print(g * 0.001)
        if met_vol_to == "Metric Tonnes":
            print(g * 0.000001)
        if met_vol_to == "Long Tonnes":
            print(g * 0.0000009842035)
        if met_vol_to == "Ounces":
            print(g * 0.03527396)
        if met_vol_to == "Pounds":
            print(g * 0.002204623)
        if met_vol_to == "Short Tonnes":
            print(g * 0.000001102311)
    if met_vol_from == "Kilogrammes":
        kg = float(input("What is your measurement? "))
        if met_vol_to == "Milligrammes":
            print(kg * 1000000)
        if met_vol_to == "Grammes":
            print(kg * 1000)
        if met_vol_to == "Metric Tonnes":
            print(kg * 0.001)
        if met_vol_to == "Long Tonnes":
            print(kg * 0.0009842035)
        if met_vol_to == "Ounces":
            print(kg * 35.27396)
        if met_vol_to == "Pounds":
            print(kg * 2.204623)
        if met_vol_to == "Short Tonnes":
            print(kg * 0.001102311)
    if met_vol_from == "Metric Tonne":
        mt = float(input("What is your measurement? "))
        if met_vol_to == "Milligrammes":
            print(mt * 1000000000)
        if met_vol_to == "Grammes":
            print(mt * 1000000)
        if met_vol_to == "Kilogrammes":
            print(mt * 1000)
        if met_vol_to == "Long Tonnes":
            print(mt * 0.9842035)
        if met_vol_to == "Ounces":
            print(mt * 35273.96)
        if met_vol_to == "Pounds":
            print(mt * 2204.623)
        if met_vol_to == "Short Tonnes":
            print(mt * 1.10231122)
    if met_vol_from == "Long Tonnes":
        lt = float(input("What is your measurement? "))
        if met_vol_to == "Milligrammes":
            print(lt * 1016050200)
        if met_vol_to == "Grammes":
            print(lt * 1016050.2)
        if met_vol_to == "Kilogrammes":
            print(lt * 1016.0502)
        if met_vol_to == "Metric Tonnes":
            print(lt * 1.0160502)
        if met_vol_to == "Ounces":
            print(lt * 35840.1162)
        if met_vol_to == "Pounds":
            print(lt * 2240.00726)
        if met_vol_to == "Short Tonnes":
            print(lt * 1.120003)