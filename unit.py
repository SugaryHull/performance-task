#Imports other conversion files as libraries
import volume
import temperature as temp
import length
import weight

#This function determines what type of conversion to do based on user input
def converter():
    unit_type = str(input("What is your unit type? (Temperature, Volume, Length, Weight) "))
    if unit_type == "Temperature":
        temp.temperature()
    elif unit_type == "Volume":
        volume.vol()
    elif unit_type == "Length":
        length.length()
    elif unit_type == "Weight":
        weight.weight()
    else:
        print("Please input a valid unit type.")
        converter()
#Executes function defined above
converter()