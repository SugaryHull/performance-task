#Imports metriclength.py as a library for metric conversions
import metriclength
def length():
    ask_met_unit = input("Is your unit metric? (Yes, No): ")
    if ask_met_unit == "Yes":
        metriclength.length()   #Executes metric conversion function from library if true
    else:
        len_unit_from = input("What unit are you converting from? (Inches, Feet, Yards, Miles): ")
        len_unit_to = input("What unit are you converting to? (Inches, Feet, Yards, Miles, Millimetres, Centimetres, Metres, Kilometres): ")
        if len_unit_from == "Inches":
            inches = float(input("What is your measurement? "))
            if len_unit_to == "Feet":
                print(inches * 0.08333333)
            if len_unit_to == "Yards":
                print(inches * 0.02777778)
            if len_unit_to == "Miles":
                print(inches * 0.00001578283)
            if len_unit_to == "Millimetres":
                print(inches * 25.4)
            if len_unit_to == "Centimetres":
                print(inches * 2.54)
            if len_unit_to == "Metres":
                print(inches * 0.0254)
            if len_unit_to == "Kilometres":
                print(inches * 0.0000254)
        if len_unit_from == "Feet":
            feet = float(input("What is your measurement? "))
            if len_unit_to == "Inches":
                print(feet * 12)
            if len_unit_to == "Yards":
                print(feet * 0.3333333)
            if len_unit_to == "Miles":
                print(feet * 0.0001893939)
            if len_unit_to == "Millimetres":
                print(feet * 304.8)
            if len_unit_to == "Centimetres":
                print(feet * 30.48)
            if len_unit_to == "Metres":
                print(feet * 0.3048)
            if len_unit_to == "Kilometres":
                print(feet * 0.0003048)
        if len_unit_from == "Yards":
            yards = float(input("What is your measurement? "))
            if len_unit_to == "Inches":
                print(yards * 36)
            if len_unit_to == "Feet":
                print(yards * 3)
            if len_unit_to == "Miles":
                print(yards * 0.0005681818)
            if len_unit_to == "Millimetres":
                print(yards * 914.4)
            if len_unit_to == "Centimetres":
                print(yards * 91.44)
            if len_unit_to == "Metres":
                print(yards * 0.9144)
            if len_unit_to == "Kilometres":
                print(yards * 0.0009144)
        if len_unit_from == "Miles":
            miles = float(input("What is your measurement? "))
            if len_unit_to == "Inches":
                print(miles * 63360)
            if len_unit_to == "Feet":
                print(miles * 5280)
            if len_unit_to == "Yards":
                print(feet * 1760)
            if len_unit_to == "Millimetres":
                print(feet * 1609344)
            if len_unit_to == "Centimetres":
                print(feet * 160934.4)
            if len_unit_to == "Metre":
                print(feet * 1609.344)
            if len_unit_to == "Kilometres":
                print(feet * 1.609344)