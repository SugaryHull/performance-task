#Imports metweight.py as a library for metric weight conversions
import metweight
def weight():
    metric = input("Is your unit metric? (Yes/No): ")
    if metric == "Yes":
        metweight.weight()  #Executes metric weight conversion function if true
    else:
        vol_from = input("What is your unit? (Ounces, Pounds, Short Tonnes): ")
        vol_to = input("What are you converting to? (Ounces, Pounds, Short Tonnes, Milligrammes, Grammes, Kilogrammes, Metric Tonnes, Long Tonnes): ")
        if vol_from == "Ounces":
            oz = float(input("What is your measurement? "))
            if vol_to == "Pounds":
                print(oz * 0.0625)
            if vol_to == "Milligrammes":
                print(oz * 28349.52)
            if vol_to == "Short Tonnes":
                print(oz * 0.00003124999)
            if vol_to == "Grammes":
                print(oz * 28.34952)
            if vol_to == "Kilogrammes":
                print(oz * 0.02834952)
            if vol_to == "Metric Tonnes":
                print(oz * 0.00002834952)
            if vol_to == "Long Tonnes":
                print(oz * 0.0000279017)
        if vol_from == "Pounds":
            lbs = float(input("What is your measurement? "))
            if vol_to == "Ounces":
                print(lbs * 16)
            if vol_to == "Short Tonnes":
                print(lbs * 0.0004999999)
            if vol_to == "Milligrammes":
                print(lbs * 453592.4)
            if vol_to == "Grammes":
                print(lbs * 453.5924)
            if vol_to == "Kilogrammes":
                print(lbs * 0.4535924)
            if vol_to == "Metric Tonnes":
                print(lbs * 0.0004535924)
            if vol_to == "Long Tonnes":
                print(lbs * 0.0004464272)
        if vol_from == "Short Tonnes":
            st = float(input("What is your measurement? "))
            if vol_to == "Ounces":
                print(st * 32000.01)
            if vol_to == "Pounds":
                print(st * 2000.001)
            if vol_to == "Milligrammes":
                print(st * 907185000)
            if vol_to == "Grammes":
                print(st * 907185)
            if vol_to == "Kilogrammes":
                print(st * 907.185)
            if vol_to == "Metric Tonnes":
                print(st * 0.907185)
            if vol_to == "Long Tonnes":
                print(st * 0.8928547)