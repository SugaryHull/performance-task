#List of valid units
temp_unit_list = ["Centigrade", "Kelvin", "Fahrenheit"]
def temperature():
#Code for temperature conversions
    temp_unit_from = str(input("What unit are you converting from? (Kelvin, Centigrade, Fahrenheit) "))
    if temp_unit_from == "Kelvin":
        K = float(input("What is your measurement? "))
        temp_unit_to = str(input("What unit are you converting to? "))
        for i in temp_unit_list:
            while temp_unit_to != i:
                if temp_unit_to == "Centigrade":
                    C = K + 274.15
                    print(C)
                    return(C)   #Return statements necessary for loop termination
                if temp_unit_to == "Fahrenheit":
                    F = K + 255.9278
                    print(F)
                    return(F)
    if temp_unit_from == "Fahrenheit":
        temp_unit_to = str(input("What unit are you converting to? "))
        F = float(input("What is your measurement? "))
        for i in temp_unit_list:
            while temp_unit_to != i:
                if temp_unit_to == "Centigrade":
                    C = (F - 32) * 5/9
                    print(C)
                    return(C)
                elif temp_unit_to == "Kelvin":
                    K = (F + 255.9278)
                    print(K)
                    return(K)
    if temp_unit_from == "Centigrade":
        temp_unit_to = str(input("What unit are you converting to? "))
        C = float(input("What is your measurement? "))
        if temp_unit_to == "Fahrenheit":
            F = (C * 1.8) + 32
            print(F)
            return(F)
        if temp_unit_to == "Kelvin":
            K = (C + 274.15)
            print(K)
            return(K)