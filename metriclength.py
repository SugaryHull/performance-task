#Defines function for metric length conversions
def length():
    len_unit_from = input("What are you converting from? (Millimetres, Centimetres, Metres, Kilometres): ")
    len_unit_to = input("What are you converting to? (Inches, Feet, Yards, Miles, Millimetres, Centimetres, Metres, Kilometres): ")
    if len_unit_from == "Millimetres":
        millimetres = float(input("What is your measurement?"))
        if len_unit_to == "Centimetres":
            print(millimetres * 0.1)
        if len_unit_to == "Metres":
            print(millimetres * 0.001)
        if len_unit_to == "Kilometres":
            print(millimetres * 0.000001)
        if len_unit_to == "Inches":
            print(millimetres * 0.03937008)
        if len_unit_to == "Feet":
            print(millimetres * 0.00328084)
        if len_unit_to == "Yards":
            print(millimetres * 0.001093613)
        if len_unit_to == "Miles":
            print(millimetres * 0.000000621371022727)
    if len_unit_from == "Centimetres":
        centimetres = float(input("What is your measurement? "))
        if len_unit_to == "Millimetres":
            print(centimetres * 100)
        if len_unit_to == "Metres":
            print(centimetres * 0.01)
        if len_unit_to == "Kilometres":
            print(centimetres * 0.00001)
        if len_unit_to == "Inches":
            print(centimetres * 0.3937008)
        if len_unit_to == "Feet":
            print(centimetres * 0.0328084)
        if len_unit_to == "Yards":
            print(centimetres * 0.01093613)
        if len_unit_to == "Miles":
            print(centimetres * 0.000006213712)
    if len_unit_from == "Metres":
        metres = float(input("What is your measurement? "))
        if len_unit_to == "Millimetres":
            print(metres * 1000)
        if len_unit_to == "Centimetres":
            print(metres * 100)
        if len_unit_to == "Kilometres":
            print(metres * .001)
        if len_unit_to == "Inches":
            print(metres * 39.37008)
        if len_unit_to == "Feet":
            print(metres * 3.28084)
        if len_unit_to == "Yards":
            print(metres * 1.093613)
        if len_unit_to == "Miles":
            print(metres * 0.0006213712)
    if len_unit_from == "Kilometres":
        kilometres = float(input("What is your measurement? "))
        if len_unit_to == "Millimetres":
            print(kilometres * 1000000)
        if len_unit_to == "Centimetres":
            print(kilometres * 100000)
        if len_unit_to == "Metres":
            print(kilometres * 1000)
        if len_unit_to == "Inches":
            print(kilometres * 39370.08)
        if len_unit_to == "Feet":
            print(kilometres * 3280.84)
        if len_unit_to == "Yards":
            print(kilometres * 1093.613)
        if len_unit_to == "Miles":
            print(kilometres * 0.6213712)